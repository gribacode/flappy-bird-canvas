const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

const maxSpeed = 2.5;

const world = {
  x: 0,
  y: 0,
  width: canvas.offsetWidth,
  height: canvas.offsetHeight,
  speed: 0,
  score: 0,
  topScore: 0,
  img: new Image(),
  draw() {
    ctx.drawImage(this.img, this.x, this.y, this.width, this.height);
  },
  drawScore() {
    ctx.fillStyle = "white";
    ctx.font = "24px serif";
    ctx.fillText(`score: ${this.score}`, 20, 40);
    ctx.fillText(`top score: ${this.topScore}`, 20, 60);
  },
};

world.img.src = "./assets/world.jpg";

function Tube(x) {
  this.x = x;
  this.y = 0;
  this.width = 65;
  this.height = 150;
  this.distance = 150;
  this.img = new Image();
  this.img.src = "./assets/tube.png";
  this.draw = () => {
    ctx.drawImage(
      this.img,
      this.x,
      this.y - this.img.height + this.height,
      this.width,
      this.img.height,
    );
    ctx.drawImage(
      this.img,
      this.x,
      this.y + this.height + this.distance,
      this.width,
      this.img.height,
    );
  };
}

const tubeArray = [new Tube(world.width), new Tube(world.width * 1.6)];

const bird = {
  x: world.width / 2 - 55,
  y: world.height / 2 - 40,
  width: 55,
  height: 40,
  gravity: 0,
  img: new Image(this.width, this.height),
  draw() {
    ctx.translate(this.x, this.y);
    ctx.rotate(0.1 * this.gravity);
    ctx.drawImage(this.img, 0, 0, this.width, this.height);
    ctx.rotate(-0.1 * this.gravity);
    ctx.translate(-this.x, -this.y);
  },
  fly() {
    this.gravity = -4.5;
    if (!world.speed) {
      world.speed = 1;
    }
  },
};

function reset() {
  bird.y = world.height / 2 - 40;
  bird.gravity = 1;

  world.speed = 0;
  world.topScore = Math.max(world.score, world.topScore);
  world.score = 0;

  tubeArray.forEach((tube) => {
    tube.x = tube.x + world.width / 1.6 + 65;
  });
}

function render() {
  world.draw();

  tubeArray.forEach((tube) => {
    tube.x = tube.x - world.speed;
    tube.draw();
    if (tube.x + tube.width < 0) {
      tube.height = Math.random() * 250 + 50;
      tube.x = world.width;
      world.speed = Math.min(world.speed + 0.25, maxSpeed);
      world.score = world.score + 1;
    }
    if (tube.x - tube.width < bird.x && tube.x + tube.width > bird.x) {
      if (tube.height > bird.y || bird.y + bird.height > tube.height + tube.distance) {
        reset();
      }
    }
  });
  bird.draw();
  
  if (world.speed) {
    bird.y = bird.y + bird.gravity;
    bird.gravity = bird.gravity + 0.3;
  }

  if (bird.y > world.height || bird.y < 0) {
    reset();
  }

  world.drawScore();
}

setInterval(render, 1000 / 60);

bird.img.src = "./assets/bird.png";
addEventListener("click", () => {
  bird.fly();
});

bird.img.src = "./assets/bird.png";
addEventListener("keydown", () => {
  bird.fly();
});
